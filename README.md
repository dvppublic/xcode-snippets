# XCode Snippets

Date: 14.03.2018

Swift Version: Swift 4.

Description: Custom code snippets - shared publicly. Handy to be shared between team members for common implementation style of common tasks. Also convenient for use when changing machines (new Mac).

How to use the code snippets:

1. Download any(/all) files from this directory with extension ".codesnippet".
2. Go to "~/Library/Developer/Xcode/UserData" & "/CodeSnippets". (If /CodeSnippet folder is not found, you need to create it)
3. Paste the files in the directory above.
4. Restart XCode if it is already running. (The custom code snippets should be added to XCode code snippets' tab)

# Useful links:
- Other Swift Snippets: https://github.com/burczyk/XcodeSwiftSnippets
- One Code Snippets tutorial: https://medium.com/@abhimuralidharan/ios-tips-creating-custom-code-snippets-in-xcode-d91facf5e242
- Another Code Snippet tutorial: https://marcosantadev.com/swift-code-snippets-xcode/
- Common issue with creating custom code snippets: https://stackoverflow.com/questions/6206380/cant-drag-to-make-code-snippet-in-xcode